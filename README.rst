===============
Python CI Image
===============



.. image:: https://gitlab.com/paul_armstrong/python_ci_image/badges/main/pipeline.svg
        :target: https://gitlab.com/paul_armstrong/python_ci_image/-/commits/main
        :alt: Pipeline Status


Pre built docker image containing all python libraries required to perform general CI checks. Create to reduce minutes spent in CI pipelines.


* Free software: MIT license


Features
--------

Created to reduce CI minute spend by pre building a minimized python library, main libraries included are:

* Black_ : Python code linting.
* Flake8_ : Python code linting
* Mypy_ : Python static type checker
* Pylint_ : Python linter
* Pytest_ : Python code testing library.
* Pytest-cov_ : Py-test plugin to provide coverage reports, required to display coverage in the repo.
* Xenon_ : Python complexity checker
* Vulture_ : Checks for unused python code

.. _Black: https://pypi.org/project/black/
.. _Flake8: https://pypi.org/project/flake8/
.. _Mypy: https://pypi.org/project/mypy/
.. _Pylint: https://pypi.org/project/pylint/
.. _Pytest: https://pypi.org/project/pytest/
.. _Pytest-cov: https://pypi.org/project/pytest-cov/
.. _Xenon: https://pypi.org/project/xenon/
.. _Vulture: https://pypi.org/project/vulture/

Credits
-------

This package was created with Cookiecutter_ and forked from `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://gitlab.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://gitlab.com/audreyr/cookiecutter-pypackage

